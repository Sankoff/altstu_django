from django.shortcuts import render,redirect, get_object_or_404
from .models import *
from django.views.decorators.csrf import csrf_exempt
# Create your views here.

@csrf_exempt
def index(request):
    c = Cloth.objects.all()
    t = Type.objects.all()
    p = Producer.objects.all()

    if request.method == 'POST':
        re_data = request.POST

        #Добавление
        if re_data['_method'] == "POST":
            t = get_object_or_404(Type, ID_Type=re_data['ID_type'])
            p = get_object_or_404(Producer, ID_Producer = re_data['ID_producer'])
            Cloth.objects.create(Name=re_data['name'], Cost=re_data['price'], Type = t, Producer = p)
            return redirect('index')

        #Удаление
        if re_data['_method'] == "DELETE":
            delete_id = re_data['new_id']
            data = Cloth.objects.get(ID_Cloth=delete_id)
            data.delete()
            return redirect('index')

        # Редактирование
        if re_data['_method'] == "PUT":
            t = get_object_or_404(Type, ID_Type=re_data['ID_type'])
            p = get_object_or_404(Producer, ID_Producer=re_data['ID_producer'])
            Cloth.objects.filter(ID_Cloth=re_data['new_id']).update(Name=re_data['name'],
                                                                           Cost=re_data['cost'],
                                                                           Type=t, Producer = p)
            return redirect('index')

    return render(request, 'clothes/index.html',{'clothes':c, 'type':t, 'producer':p})
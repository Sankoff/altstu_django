from django.contrib import admin
from clothes.models import Cloth
from clothes.models import Producer
from clothes.models import Type

admin.site.register(Cloth)
admin.site.register(Producer)
admin.site.register(Type)
# Register your models here.

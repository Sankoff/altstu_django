from django.test import TestCase
from clothes.models import Producer
from clothes.models import Type
from clothes.models import Cloth


class ProducerModelTest(TestCase):
    """Тесты для модели производителя одежды"""
    @classmethod
    def setUpTestData(cls):
        """ Задаем данные для тестов к модели"""
        Producer.objects.create(Name='Adidas', Country='America')

    def test_name_label(self):
        """Тестируем наличие поля"""
        producer = Producer.objects.get(ID_Producer=2)
        field_label = producer._meta.get_field('Country').verbose_name
        self.assertEquals(field_label, 'Страна')

    def test_name_max_length(self):
        """Тестируем макс длину поля"""
        producer = Producer.objects.get(ID_Producer=2)
        max_length = producer._meta.get_field('Country').max_length
        self.assertEquals(max_length, 255)

    def test_producer_name(self):
        """Тестируем на содержимое поля Наименования"""
        producer = Producer.objects.get(ID_Producer=2)
        self.assertEquals(producer.Name, 'Adidas')

    def test_producer_country(self):
        """Тестируем на содержимое поля Страны"""
        producer = Producer.objects.get(ID_Producer=2)
        self.assertEquals(producer.Country, 'America')

class TypeModelTest(TestCase):
    """Тесты для модели Вида одежды"""
    @classmethod
    def setUpTestData(cls):
        """ Задаем данные для тестов к модели"""
        Type.objects.create(Name='Кроссовки')

    def test_type_name_label(self):
        """Тестируем наличие поля"""
        _type = Type.objects.get(ID_Type=2)
        field_label = _type._meta.get_field('Name').verbose_name
        self.assertEquals(field_label, 'Наименование')

    def test_type_name_max_length(self):
        """Тестируем макс длину поля"""
        _type = Type.objects.get(ID_Type=2)
        max_length = _type._meta.get_field('Name').max_length
        self.assertEquals(max_length, 255)

    def test_type_name(self):
        """Тестируем на содержимое поля Наименования"""
        _type = Type.objects.get(ID_Type=2)
        self.assertEquals(_type.Name, 'Кроссовки')


class ClothModelTest(TestCase):
    """Тесты для модели одежда"""
    @classmethod
    def setUpTestData(cls):
        """ Задаем данные для тестов к модели"""
        producer = Producer.objects.create(Name='Adidas', Country='America')
        _type = Type.objects.create(Name='Кроссовки')
        Cloth.objects.create(Name='Кроссовки Adidas красные', Cost=3000, Type=_type, Producer=producer, thumb=None)

    def test_name_label(self):
        """Тестируем наличие поля"""
        cloth = Cloth.objects.get(ID_Cloth=1)
        field_label = cloth._meta.get_field('Cost').verbose_name
        self.assertEquals(field_label, 'Cost')

    def test_name_max_length(self):
        """Тестируем макс длину поля"""
        cloth = Cloth.objects.get(ID_Cloth=1)
        max_length = cloth._meta.get_field('Name').max_length
        self.assertEquals(max_length, 255)

    def test_clothes_producer(self):
        """Тестируем на содержимое поля Производителя"""
        cloth = Cloth.objects.get(ID_Cloth=1)
        producer_name = cloth.Producer.Name
        self.assertEquals(producer_name, 'Adidas')

    def test_clothes_type(self):
        """Тестируем на содержимое поля Вида"""
        cloth = Cloth.objects.get(ID_Cloth=1)
        type_name = cloth.Type.Name
        self.assertEquals(type_name, 'Кроссовки')

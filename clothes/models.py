from django.db import models


# Create your models here.

class Producer(models.Model):
    ID_Producer = models.AutoField(primary_key=True)
    Name = models.CharField(verbose_name='Наименование', max_length=255)
    Country = models.CharField(verbose_name='Страна', max_length=255)

    class Meta:
        db_table = 'producer'
        verbose_name = 'Производитель'
        verbose_name_plural = 'Производители'


class Type(models.Model):
    ID_Type = models.AutoField(primary_key=True)
    Name = models.CharField(verbose_name='Наименование', max_length=255)

    class Meta:
        db_table = 'type'
        verbose_name = "Вид"
        verbose_name_plural = 'Виды'


class Cloth(models.Model):
    ID_Cloth = models.AutoField(primary_key=True)
    Name = models.CharField(max_length=255)
    Cost = models.IntegerField()
    Type = models.ForeignKey(Type, models.DO_NOTHING)
    Producer = models.ForeignKey(Producer, models.DO_NOTHING)
    thumb = models.ImageField(default='default.png', blank=True)

    class Meta:
        db_table = 'cloth'
        verbose_name = 'Одежда'
        verbose_name_plural = 'Одежда'

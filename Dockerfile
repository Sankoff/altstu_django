FROM python:3.8
ENV PYTHONUNBUFFERED 1
RUN mkdir /altstu_django
WORKDIR /altstu_django
COPY ./ ./
RUN pwd
RUN chmod -R 777 ./
RUN pip install --upgrade pip && pip install -r req.txt
ADD . /altstu_django/